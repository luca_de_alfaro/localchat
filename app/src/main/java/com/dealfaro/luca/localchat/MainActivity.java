package com.dealfaro.luca.localchat;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public class MainActivity extends AppCompatActivity {

    Location lastLocation;
    private double lastAccuracy = (double) 1e10;
    private long lastAccuracyTime = 0;
    private static final String LOG_TAG = "localchat";

    private static final String BASE_URL = "https://attendancerecorder.appspot.com/";

    // Interface for uploading location.
    public interface LocationCall {
        @FormUrlEncoded
        @POST("attendance/default/checkin.json")
        Call<String> reportLocation(
                @Field("lat") String lat,
                @Field("lng") String lng,
                @Field("acc") String acc
        );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    @Override
    protected void onPause() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.removeUpdates(locationListener);
        super.onPause();
    }

    /**
     * Listenes to the location, and gets the most precise recent location.
     */
    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            double newAccuracy = location.getAccuracy();
            Log.i(LOG_TAG, "Accuracy is " + newAccuracy);
            long newTime = location.getTime();
            // Is this better than what we had?  We allow a bit of degradation in time.
            boolean isBetter = ((lastLocation == null) ||
                    newAccuracy < lastAccuracy + (newTime - lastAccuracyTime));
            if (isBetter) {
                // We replace the old estimate by this one.
                lastLocation = location;
                lastAccuracy = location.getAccuracy();
                lastAccuracyTime = location.getTime();
                // We display the accuracy.
                displayAccuracy(location);
            }
        }



        public void sendLocation(View v) {
            // Then, we build the interface.
            Gson receiverGson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(receiverGson))
                    .build();



        }


        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onProviderDisabled(String provider) {}
    };


    /**
     * Displays the accuracy to the user.
     * @param location
     */
    private void displayAccuracy(Location location) {
        // Displays the accuracy.
        TextView accView = (TextView) findViewById(R.id.accView);
        if (location == null) {
            accView.setText(R.string.no_location);
        } else {
            String acc = String.format("%5.1f m", location.getAccuracy());
            accView.setText(acc);
        }
    }


}
